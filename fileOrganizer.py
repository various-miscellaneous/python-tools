#!/usr/bin/python3
# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.
import os


def find_matching_files(path):
	"""
	Organizes all files in a dictionary based on the filename without extension.
	:param path:
	:return files dictionary:
	"""
	file_dict = {}
	for item in os.listdir(path):
		if os.path.isfile(os.path.join(path, item)):
			file = item
			last_dot_index = file.rfind('.')
			if last_dot_index > 0:
				ext = file[last_dot_index:]
				name = file[:last_dot_index]
				if name in file_dict:
					files = file_dict[name]
				else:
					files = []
					file_dict[name] = files
				files.append((name, ext))
	return file_dict


dir_path = os.getcwd()
dictionary = find_matching_files(dir_path)

# creates folders for all the dictionary items and moves all the associated files on to their respective folder.
for file in dictionary:
	os.makedirs(file)
	for file_item in dictionary[file]:
		file_name = file+file_item[1]
		os.rename(file_name, file + "/" + file_name)