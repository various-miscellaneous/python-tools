#!/usr/bin/python3
# MIT License
#
# Copyright (c) 2019 Magnus Jørgensen
#
# Permission is hereby granted, free of charge, to any person obtaining a copy
# of this software and associated documentation files (the "Software"), to deal
# in the Software without restriction, including without limitation the rights
# to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
# copies of the Software, and to permit persons to whom the Software is
# furnished to do so, subject to the following conditions:
#
# The above copyright notice and this permission notice shall be included in all
# copies or substantial portions of the Software.
#
# THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
# IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
# FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
# AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
# LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
# OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
# SOFTWARE.

import os
import sys

# This script removes the last x characters from all files in the Current Working Directory (cwd).
# Such that say "File Name-3849764.ext" -> "File Name.ext"


def remove_middle(path, length):
	"""
	:param path: The path to work on
	:param length: Length to remove from middle of filename
	:return:
	"""
	path_length = len(path)+1
	files = []
	for r, d, f in os.walk(path):
		for file in f:
			last_dot_index = file.rfind('.')
			if last_dot_index > 0:
				ext = file[last_dot_index:]
				name = file[:last_dot_index-length]
				origin = os.path.join(r[path_length:], file)
				new = os.path.join(r[path_length:], name + ext)
				files.append((origin, new))
	return files


dirpath = os.getcwd()
length_to_remove = int(sys.argv[1])
files = remove_middle(dirpath, length_to_remove)

for file in files:
	print(file)
	#os.rename(file[0], file[1]) # Uncomment this line to make it work
